// crud makanan
const makananRepo = (db) => ({
    all: () => db.makanan,
    // get makanan by its id
    get: (id) => db.makanan.filter(m => m.id === id)
})

module.exports = makananRepo

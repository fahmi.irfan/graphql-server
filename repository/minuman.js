// crud minuman
const minumanRepo = (db) => ({
    all: () => db.minuman,
    get: (id) => db.minuman.filter(m => m.id === id)
})

module.exports = minumanRepo

const fs = require('fs')
const path = require('path')

const dbFile = fs.readFileSync(path.join(__dirname, 'db.json'));
// parse string of json into JSON
const db = JSON.parse(dbFile.toString())

module.exports = db
const express = require('express')
const graphqlHTTP = require('express-graphql')
const { buildSchema } = require('graphql')
const path = require('path')
const db = require('./database/db')
const minuman = require('./repository/minuman')
const makanan = require('./repository/makanan')

const makananRepo = makanan(db)
const minumanRepo = minuman(db)

// Construct schema, using GraphQL schema language
const schema = buildSchema(`
    type Makanan {
        id: Int
        nama: String
        level: [Int]
    }

    type Minuman {
        id: Int
        nama: String
        cupSize: [String]
    }

    type Query {
        getMakanan(id: Int): [Makanan]
        getMinuman(id: Int): [Minuman]
    }
`)

// rootValue provides a resolver function for each API endpoint
// resolver state what todo when a query get called
const rootValue = {
    getMakanan: ({ id }) => {
        if (!id) return makananRepo.all()
        if (id > 0) return makananRepo.get(id)
    },
    getMinuman: ({ id }) => {
        if (!id) return minumanRepo.all()
        if (id > 0) return minumanRepo.get(id)
    }
}

const app = express()
// serve static file in `/`
app.use('/', express.static(path.join(__dirname, 'static')))
app.use('/graphql', graphqlHTTP({
    schema,
    rootValue,
    graphiql: true,
}))

const PORT = 4000

app.listen(PORT)

console.log(`graphql server run on port ${PORT}`)


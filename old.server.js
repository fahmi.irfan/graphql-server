const express = require('express')
const graphqlHTTP = require('express-graphql')
const { buildSchema } = require('graphql')
const faker = require('faker')

const beritas = [];
for (let i = 0; i < 10; i++) {
    beritas.push({
        id: i+1,
        judul: faker.name.title(),
        penulis: faker.name.firstName(),
        waktuTerbit: new Date(),
        isi: faker.lorem.paragraphs(),
        jmlKomentar: faker.random.number(),  
    })
}

// Construct schema, using GraphQL schema language
const schema = buildSchema(`
    scalar Date

    type Query {
        getBerita(id: Int): [Berita]
    }

    type Berita {
        judul: String,
        penulis: String,
        waktuTerbit: Date,
        isi: String,
        jmlKomentar: Int,
    }
`)

// rootValue provides a resolver function for each API endpoint
// resolver state what todo when a query get called
const rootValue = {
    getBerita: ({ id }) => {
        const resBerita = !id ? beritas : [beritas[id - 1]]
        return resBerita
    },
}

const app = express()
app.use('/graphql', graphqlHTTP({
    schema,
    rootValue,
    graphiql: true,
}))

const PORT = 4000

app.listen(PORT)

console.log(`graphql server run on port ${PORT}`)

